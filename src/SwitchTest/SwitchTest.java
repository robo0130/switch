package SwitchTest;

import Switch.Switch;
import Switch.SwitchExceptions.*;

import java.io.InputStreamReader;
import java.util.*;

import org.jnetpcap.Pcap;
import org.jnetpcap.PcapIf;

public class SwitchTest {

	public static void main(String[] args) {		
		Switch sw;
		
		try {
			sw = new Switch();
		} catch (SwitchException e) {
			System.out.println(e.getMessage());
			return;
		}

		StringBuilder errBuff = new StringBuilder();
		List<PcapIf> allInterfaces = new ArrayList<PcapIf>();
		Pcap.findAllDevs(allInterfaces, errBuff);

		Scanner scanner = new Scanner(new InputStreamReader(System.in));
		try {
			System.out.println("Enter commands");
			while (scanner.hasNextLine()) {
				String command = scanner.nextLine();
				if (command.equals("exit"))
					break;
				else if(command.equals("show all interfaces")){
					for (int i = 0; i < allInterfaces.size(); i++)
						System.out.println("#" + i + ": "
								+ allInterfaces.get(i).getName() + " - "
								+ allInterfaces.get(i).getDescription());
				}
				else if(command.matches("add interface \\d*")){
					int index = Integer.parseInt(command.substring(14, command.length()).trim());
					sw.addInterface(index);
					System.out.println("Interface " + allInterfaces.get(index).getDescription() + " added");
				}
				else if(command.matches("switch start")){
					sw.start();
				}
				else if(command.matches("switch stop")){
					sw.stop();
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		scanner.close();
		if(sw != null)
			sw.stop();
	}
}
