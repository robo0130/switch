package Switch;

import org.jnetpcap.*;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.jnetpcap.protocol.lan.Ethernet;

import Switch.SwitchExceptions.*;

import java.util.*;

public class Switch implements PcapPacketHandler<Interface> {
	private List<Interface> interfaces;
	private List<PcapIf> allInterfaces;
	private MacTable macTable;

	public Switch() throws NoSuitablePcapDevicesException {
		allInterfaces = new ArrayList<PcapIf>();
		interfaces = new ArrayList<Interface>();
		macTable = new MacTable();

		StringBuilder errBuff = new StringBuilder();
		Pcap.findAllDevs(allInterfaces, errBuff);
		if (allInterfaces.isEmpty())
			throw new NoSuitablePcapDevicesException(errBuff.toString());
	}

	public void addInterface(int ifNum) {
		if(ifNum >= allInterfaces.size())
			throw new IndexOutOfBoundsException("Wrong device number specified.");
		
		Interface _if = new Interface(allInterfaces.get(ifNum), this);
		interfaces.add(_if);
	}

	public void start() throws SwitchException {
		for (Interface _if : interfaces) {
			_if.start();
		}
	}

	public void stop() {
		macTable.dispose();
		
		for (Interface _if : interfaces) {
			_if.stop();
		}
	}

	@Override
	public void nextPacket(PcapPacket packet, Interface _if) {
		if (_if.isOutgoingPacket(packet)) // ak je to vlastny packet IF-su tak ho ignoruje
			return;
		// debug
		// System.out.println("Received packet on " +
		// SwitchHelperMethods.getMacStringFromBytes(_if.getMacAddress()));

		Ethernet ethHeader = new Ethernet();
		packet.getHeader(ethHeader);
		byte[] srcMac = new byte[6];
		byte[] dstMac = new byte[6];
		ethHeader.sourceToByteArray(srcMac);
		ethHeader.destinationToByteArray(dstMac);

		macTable.addInterface(srcMac, _if); // naucenie mac adresy
		
		Interface dstIf = macTable.getInterface(dstMac); // ziskanie IF-su z mac tabulky podla mac adresy
		if (dstIf != null) // ak pozna cielovi IF, tak odosiela tam
			dstIf.send(packet);
		else // ak nepozna tak odosle vsade okrem seba
			for (Interface _interface : interfaces)
				if (_if != _interface)
					_interface.send(packet);
	}
}