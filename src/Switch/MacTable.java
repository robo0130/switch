package Switch;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;

import org.jnetpcap.nio.Disposable;

public class MacTable implements Disposable{
	private int timeout;
	private Timer timeoutTimer;
	private Hashtable<ByteArrayWrapper, Interface> macTable;
	private Hashtable<ByteArrayWrapper, Integer> timeoutTable;

	public MacTable() {
		timeout = 30;
		macTable = new Hashtable<ByteArrayWrapper, Interface>();
		timeoutTable = new Hashtable<ByteArrayWrapper, Integer>();
		timeoutTimer = new Timer();
		timeoutTimer.scheduleAtFixedRate(new TimeoutTask(), 0, 1000);
	}

	public Interface getInterface(byte[] mac) {
		ByteArrayWrapper wrapper = new ByteArrayWrapper(mac);
		
		synchronized (this) {
			if (macTable.containsKey(wrapper)) {
				return macTable.get(wrapper);
			}
		}

		return null;
	}

	public void addInterface(byte[] mac, Interface _if) {
		ByteArrayWrapper wrapper = new ByteArrayWrapper(mac);
		
		synchronized (this) {
			if (!macTable.containsKey(wrapper)){
				macTable.put(wrapper, _if);
			}
			else if (macTable.get(wrapper) != _if) {
				macTable.remove(wrapper);
				timeoutTable.remove(wrapper);
				macTable.put(wrapper, _if);
			}

			timeoutTable.put(wrapper, 0);
		}
	}
	
	public void dispose(){
		timeoutTimer.cancel();
	}

	private class TimeoutTask extends TimerTask {

		@Override
		public void run() {
			MacTable mtThis = MacTable.this;
			Enumeration<ByteArrayWrapper> keys = mtThis.timeoutTable.keys();
			
			while (keys.hasMoreElements()) {
				ByteArrayWrapper next = keys.nextElement();

				synchronized (this) {
					mtThis.timeoutTable.put(next, mtThis.timeoutTable.get(next) + 1);
					if (mtThis.timeoutTable.get(next) > mtThis.timeout) {
						mtThis.macTable.remove(next);
						mtThis.timeoutTable.remove(next);
					}
				}
			}
		}
	}
}