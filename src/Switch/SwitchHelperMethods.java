package Switch;

public class SwitchHelperMethods {
	public static String getMacStringFromBytes(byte[] mac){
		final StringBuilder buf;
		buf = new StringBuilder();
		for (byte b : mac) {  
			int pom = b + 128;
			if (buf.length() != 0) {  
		        buf.append(':');  
			}  
			if (pom >= 0 && pom < 16) {  
				buf.append('0');  
			}  
			buf.append(Integer.toHexString(pom).toUpperCase());  
		}  
		   
		return buf.toString();  
	}
}
