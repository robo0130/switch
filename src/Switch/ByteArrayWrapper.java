package Switch;

import java.util.Arrays;

public final class ByteArrayWrapper
{
    private final byte[] data;

    public ByteArrayWrapper(byte[] data)
    {
        if (data == null)
        {
            throw new NullPointerException();
        }
        this.data = data;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof ByteArrayWrapper))
        {
            return false;
        }
        return Arrays.equals(data, ((ByteArrayWrapper)o).data);
    }

    @Override
    public int hashCode()
    {
        return Arrays.hashCode(data);
    }
}