package Switch.SwitchExceptions;

@SuppressWarnings("serial")
public class SwitchException extends Exception{

	public SwitchException(String errMessage) {
		super(errMessage);
	}
}