package Switch.SwitchExceptions;

@SuppressWarnings("serial")
public class DeviceNotSupportedException extends SwitchException {

	public DeviceNotSupportedException(String errMessage) {
		super(errMessage);
	}

}
