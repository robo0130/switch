package Switch.SwitchExceptions;

@SuppressWarnings("serial")
public class NoSuitablePcapDevicesException extends SwitchException{

	public NoSuitablePcapDevicesException(String errMessage){
		super(errMessage);
	}
}