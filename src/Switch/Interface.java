package Switch;

import java.util.HashSet;

import org.jnetpcap.*;
import org.jnetpcap.nio.JBuffer;
import org.jnetpcap.packet.*;

import Switch.SwitchExceptions.*;

public class Interface implements Runnable {
	private PcapIf _interface;
	private Pcap pcapLiveDevice;
	private Boolean isListening;
	private Thread listenerThread;
	private HashSet<ByteArrayWrapper> sentPackets;
	private PcapPacketHandler<Interface> packetHandler;

	public Interface(PcapIf _interface, PcapPacketHandler<Interface> packetHandler) {
		this._interface = _interface;
		this.packetHandler = packetHandler;
		sentPackets = new HashSet<ByteArrayWrapper>();
		isListening = false;
	}

	public void start() throws SwitchException {
		initInterface();
		listenerThread.start();

		// debug
		try {
			System.out.println("Listening started on "
					+ SwitchHelperMethods.getMacStringFromBytes(_interface
							.getHardwareAddress()));
		} 
		catch (Exception e) {
		}
	}

	private void initInterface() throws SwitchException {
		StringBuilder errBuff = new StringBuilder();
		listenerThread = new Thread(this, _interface.getName() + " listener");
		
		try{
			pcapLiveDevice = Pcap.openLive(_interface.getName(), 0, Pcap.MODE_PROMISCUOUS, 1, errBuff);
		}
		catch(Exception e){
			throw e;
		}
		
		if (pcapLiveDevice == null)
			throw new DeviceNotSupportedException(errBuff.toString());
	}

	@SuppressWarnings({ "deprecation" })
	@Override
	public void run() {
		isListening = true;

		while (isListening) {
			pcapLiveDevice.dispatch(1, packetHandler, this); // dispatch lebo loop sa neda prerusit kym neprijde ziaden packet(ignoruje timeout)
		}
		
		System.out.println("Thread stopped");
	}

	public void send(PcapPacket packet) {
		// debug
		// System.out.println("Sending packet to "
		// + SwitchHelperMethods.getMacStringFromBytes(getMacAddress()));

		sentPackets.add(new ByteArrayWrapper(packet.getByteArray(0,	packet.size()))); // zaradi packet medzi odoslane aby sa dal pri op�tovnom prijati odfiltrovat
		pcapLiveDevice.sendPacket((JBuffer) packet);
	}

	public void stop() {
		if (isListening && pcapLiveDevice != null) {
			isListening = false;
			pcapLiveDevice.breakloop();
			
			try {
				listenerThread.join(); // caka na ukoncenie pocuvajuceho threadu
			} catch (InterruptedException e) {
				pcapLiveDevice.close();
			}
			
			pcapLiveDevice.close();
		}
	}

	public byte[] getMacAddress() {
		try {
			return _interface.getHardwareAddress();
		} catch (Exception e) {
			return null;
		}
	}

	public boolean isOutgoingPacket(PcapPacket packet) {
		ByteArrayWrapper wrapper = new ByteArrayWrapper(packet.getByteArray(0, packet.size()));

		if (sentPackets.contains(wrapper)) {
			sentPackets.remove(wrapper);
			
			return true;
		}

		return false;
	}
}